package com.homework003.spring_homework_003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprinHomework003Application {

    public static void main(String[] args) {
        SpringApplication.run(SprinHomework003Application.class, args);
    }

}
