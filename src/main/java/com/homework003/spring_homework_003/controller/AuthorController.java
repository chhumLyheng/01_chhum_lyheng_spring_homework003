package com.homework003.spring_homework_003.controller;

import com.homework003.spring_homework_003.model.enitity.Author;
import com.homework003.spring_homework_003.model.request.AuthorRequest;
import com.homework003.spring_homework_003.model.response.AuthorResponse;
import com.homework003.spring_homework_003.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/all")
    @Operation(summary ="Get all authors")
    public ResponseEntity<AuthorResponse<List<Author> >> getAllAuthor(){
        AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                .message("Fetch Data Successful")
                .payload(authorService.getAllAuthors())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();

        return ResponseEntity.ok(response);
    }
@GetMapping("/{id}")
@Operation(summary ="Get author by id")
 public ResponseEntity<AuthorResponse<Author>> getAuthorById(@PathVariable("id")Integer authorId){
    AuthorResponse<Author> response = null;
       if (authorService.getAuthorById(authorId) !=null){
           response = AuthorResponse.<Author>builder()
                   .message("Success fetch data by id")
                   .payload(authorService.getAuthorById(authorId))
                   .httpStatus(HttpStatus.OK)
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                   .build();
           return ResponseEntity.ok(response);
       }else {
           response = AuthorResponse.<Author>builder()
                   .message("Data not found")
                   .httpStatus(HttpStatus.NOT_FOUND)
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                   .build();
           return ResponseEntity.badRequest().body(response);
       }

}
@DeleteMapping("/{id}")
    @Operation(summary = "Delete author by id")
    public  ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable("id") Integer authorId){
    AuthorResponse<String> response =null;
       if(authorService.deleteById(authorId) == true){
           response = AuthorResponse.<String>builder()
                   .message("Delete successful")
                   .httpStatus(HttpStatus.OK)
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                   .build();
       }

        return ResponseEntity.ok(response);
}
@PostMapping
    @Operation(summary = "Save new author")
    public ResponseEntity<AuthorResponse<Author>> addNewAuthor(@RequestBody AuthorRequest authorRequest){
       Integer storeAuthorId = authorService.addNewAuthor(authorRequest);
        if(storeAuthorId !=null){
            AuthorResponse<Author> response =AuthorResponse.<Author>builder()
                    .message("Add success")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
}
@PutMapping("/{id}")
    @Operation(summary = "Update author by Id")
    public ResponseEntity<AuthorResponse<Author>> updateAuthorById(
            @RequestBody AuthorRequest authorRequest, @PathVariable("id") Integer authorId){
        AuthorResponse<Author> response = null;
        Integer idAuthorUpdate = authorService.updateAuthor(authorRequest, authorId);
        if(idAuthorUpdate != null){
            response = AuthorResponse.<Author>builder()
                    .message("Update successfully")
                    .payload(authorService.getAuthorById(idAuthorUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
            return  ResponseEntity.ok(response);
}

}
