package com.homework003.spring_homework_003.controller;

import com.homework003.spring_homework_003.model.enitity.Book;
import com.homework003.spring_homework_003.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
    private  final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }
    @GetMapping("/all")
    @Operation(summary = "Get all book")
    public ResponseEntity<List<Book>> getAllBooks(){
        return ResponseEntity.ok(bookService.getAllBook());
    }
    @GetMapping("/all")
    @Operation(summary = "Get all book")
    public ResponseEntity<Book> getAllBooks(@PathVariable("id") Integer bookId){
        return ResponseEntity.ok(bookService.getBookById(bookId));
    }
}

