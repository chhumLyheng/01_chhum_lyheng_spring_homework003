package com.homework003.spring_homework_003.service.serviceImpl;

import com.homework003.spring_homework_003.model.enitity.Book;
import com.homework003.spring_homework_003.repository.BookRepository;
import com.homework003.spring_homework_003.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBook() {
        return bookRepository.findAllBook();
    }

    @Override
    public Book getBookById(Integer bookId) {
        return bookRepository.getBookById(bookId);
    }
}
