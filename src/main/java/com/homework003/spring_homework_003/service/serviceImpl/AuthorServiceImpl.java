package com.homework003.spring_homework_003.service.serviceImpl;

import com.homework003.spring_homework_003.model.enitity.Author;
import com.homework003.spring_homework_003.model.request.AuthorRequest;
import com.homework003.spring_homework_003.repository.AuthorRepository;
import com.homework003.spring_homework_003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthorServiceImpl implements AuthorService {
    private  final AuthorRepository authorRepository;
    public AuthorServiceImpl(AuthorRepository authorRepository) {

        this.authorRepository = authorRepository;
    }
    @Override
    public List<Author> getAllAuthors() {

        return authorRepository.findAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer authorId) {
        return authorRepository.getAuthorById(authorId);
    }

    @Override
    public boolean deleteById(Integer authorId) {

        return authorRepository.deleteById(authorId);
    }

    @Override
    public Integer addNewAuthor(AuthorRequest authorRequest) {
        Integer authorId = authorRepository.saveAuthor(authorRequest);
        return authorId;
    }

    @Override
    public Integer updateAuthor(AuthorRequest authorRequest, Integer authorId) {
       Integer authorIdUpdate = authorRepository.updateAuthor(authorRequest, authorId);
        return authorIdUpdate;
    }


}
