package com.homework003.spring_homework_003.service;

import com.homework003.spring_homework_003.model.enitity.Author;
import com.homework003.spring_homework_003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthors();

    Author getAuthorById(Integer authorId);
    boolean deleteById(Integer authorId);
    Integer addNewAuthor(AuthorRequest authorRequest);
    Integer updateAuthor(AuthorRequest authorRequest,Integer authorId);

}
