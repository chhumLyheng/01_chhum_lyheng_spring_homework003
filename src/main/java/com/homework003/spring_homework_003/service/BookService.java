package com.homework003.spring_homework_003.service;

import com.homework003.spring_homework_003.model.enitity.Book;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();
    Book getBookById(Integer bookId);
}
