package com.homework003.spring_homework_003.repository;

import com.homework003.spring_homework_003.model.enitity.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("SELECT * FROM books WHERE book_id =#{bookId}")
    @Result(property = "id",column = "book_id")
    @Select("SELECT * FROM books")
    @Result(property = "timestamp", column = "published_date")
    @Result(property = "author", column ="author_id",
    one = @One(select = "com/homework003/spring_homework_003/repository/AuthorRepository.java:15")
    )
    @Result(property =  "categories", column = "id",
    many = @Many(select = "com.homework003.spring_homework_003.repository.CategoryRepository.getCategoryByBookId"))
     List<Book> findAllBook();

    Book getBookById(Integer bookId);
}
