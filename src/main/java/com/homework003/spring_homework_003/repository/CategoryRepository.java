package com.homework003.spring_homework_003.repository;

import jdk.jfr.Category;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("SELECT c.id,y c.name FROM book_details bd "+
    "INNER JOIN categories c ON c.id = bd.category_id "+
    "WHERE bd.book_id = #{bookId}")
    List<Category> getCategoryByBookId(Integer bookId);
}
