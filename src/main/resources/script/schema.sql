create table authors(
    author_id SERIAL PRIMARY KEY,
    author_name VARCHAR(50),
    gender VARCHAR(6)
);
create table categories(
    category_id SERIAL PRIMARY KEY ,
    category_name VARCHAR(255) NOT NULL
);
create table books (
    book_id SERIAL PRIMARY KEY ,
    title VARCHAR(255) NOT NULL ,
    published_date TIMESTAMP,
    author_id  INT REFERENCES authors(author_id)
);
create  table book_details(
    book_id INT REFERENCES books(book_id) ON UPDATE CASCADE ON DELETE CASCADE ,
    category_id INT REFERENCES categories(category_id) ON UPDATE CASCADE ON DELETE CASCADE ,
    PRIMARY KEY (book_id, category_id)
);